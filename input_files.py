#Specify path to different files needed

#node biyection between Tranus and Visum IDS
biyection_file = 'Input_Files/biyection_tranus_visum.csv'   

#Tranus link file
links_file = 'Input_Files/NEW-00A.links'					

#assignement Tranus file, output of tranus
assignement_file = 'Input_Files/assignement_fausto.csv'		

#Tranus network written with Visum Ids, this file is not mandatory, will be
#created form links_file and biyection file if needed/
tranus_network_file = 'Input_Files/tranus_visum_attributes.csv'	

#Visum netwrok file, a list of links.
visum_network_file = 'Input_Files/adj_visum2015.csv'